const express = require("express");
const router = express.Router();

router.get("/", async (req, res) => {
  try {
    const protocol = req.protocol;
    const host = req.hostname;
    const url = req.originalUrl;
    const fullUrl = `${protocol}://${host}${url}`
    console.log("req.fullUrl: ", fullUrl);
    res.json({
      status: 200,
      message: fullUrl,
    });
  } catch (error) {
    console.error(error);
    return res.status(500).send("Server error");
  }
});

module.exports = router;
